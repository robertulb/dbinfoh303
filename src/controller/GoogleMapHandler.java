package controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.AnchorPane;
import model.MarkerFactory;
import model.Scooter;

import java.util.ArrayList;
import java.util.List;

public class GoogleMapHandler implements MapComponentInitializedListener {

    private AnchorPane googleMapHodler;
    private GoogleMapView googleMapView;
    private GoogleMap map;
    private static ObservableList<Scooter> scooters = FXCollections.observableArrayList();

    private static  ObservableList<Marker> markers;

    public GoogleMapHandler(AnchorPane googleMapHodler) {
        this.googleMapHodler = googleMapHodler;
        this.googleMapView = new GoogleMapView("en-US",
                "AIzaSyC7t-jSo-huNGoyxN3hlFIBrvLiZGZorPA");
        this.markers = FXCollections.observableArrayList();
    }


    public void setMapView(){
        googleMapHodler.getChildren().add(googleMapView);
        googleMapView.setPrefHeight(470);
        googleMapView.setPrefWidth(700);
        googleMapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized() {
        System.out.println("google map init");
        setMapInit();
        setUpMapMarker();
        setUpMapScooters();
    }

    private void setMapInit() {
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(50.8503 , 4.3517)).mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false).panControl(false).rotateControl(false).scaleControl(false)
                .streetViewControl(false).zoomControl(false).zoom(12);
        map = googleMapView.createMap(mapOptions);
    }

    private void setUpMapScooters(){
        scooters.addListener((ListChangeListener<Scooter>) c -> {
            System.out.println("Number of scooters on the map " + markers.size());
            while (c.next()){
                int start = c.getFrom() ; int end = c.getTo() ;
                for (int i = start ; i < end ; i++) {
                    Scooter scooter = c.getList().get(i);
                   markers.add(MarkerFactory.getInstance().build(scooter.getPosition().getLatitude(),
                            scooter.getPosition().getLongitude(), scooter.getScooterID()));
                    System.out.println(  scooter.getScooterID());
                }
            }
        });
    }

    private void setUpMapMarker(){
        markers.addListener((ListChangeListener<Marker>) c -> {
            System.out.println(" map ok");
            while (c.next()){
                int start = c.getFrom() ; int end = c.getTo() ;
                for (int i = start ; i < end ; i++) {
                    map.addMarker(c.getList().get(i));
                    System.out.println(  c.getList().get(i).getTitle());
                }
            }
        });



        // FIXME: 4/20/19 to be removed
        markers.add(MarkerFactory.getInstance().build(50.86233,  4.37575, "Scooter1"));
        markers.add(MarkerFactory.getInstance().build(50.86946, 4.36523, "Scooter2"));
        markers.add(MarkerFactory.getInstance().build(50.86633, 4.37391, "Scooter3"));
        markers.add(MarkerFactory.getInstance().build(50.82679, 4.36688, "Scooter4"));
    }



    public static void setScooters(ObservableList<Scooter> scooters) {
        GoogleMapHandler.scooters = scooters;
    }
}
