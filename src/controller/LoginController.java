package controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.GuilURL;
import model.db.UserDAO;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author yveskerbensdeclerus
 */
public class LoginController {

    /**
     * textfield used to let the user enter his username
     */
    @FXML
    private TextField usrLogin;

    /**
     * textfield used to let the user enter his password
     */
    @FXML
    private TextField passwdLogin;

    /**
     * text set in case of problem during the connection
     */
    @FXML
    private Text errorLogin;

    /**
     * icon of the first view of the application
     */
    @FXML
    private ImageView loginMainIcon;

    /**
     * Grid holding the view
     */
    @FXML
    private GridPane fieldGridPane;


    /**
     * method to initialise the view: hide the error text, display the icon and display the  grid lines
     */
    public void  initialize() {
        errorLogin.setVisible(false);
        fieldGridPane.setGridLinesVisible(false);
    }

    @FXML
    public void  launchAdminView(ActionEvent event) throws IOException {
        WindowSwitcher.getInstance().switchWindow(event, GuilURL.MACHANICIAN_URL, "Mechanic Portal", false);
    }
    /**
     * Allow us to access the registration view
     */
    @FXML
    public void switchToRegistrationWindow(ActionEvent event) throws IOException {
        WindowSwitcher.getInstance().switchWindow(event, GuilURL.REGISTER_URL, "Register", false);
    }

    /**
     * Allow us to switch from the login to the main window.
     * A verification to verify that the username and the password are correct is proceeded
     */
    @FXML
    public void switchToMainWindow (ActionEvent event) throws IOException, SQLException {
        List<String> userLogInfo = UserDAO.getInstance().authenticate(usrLogin.getText(), passwdLogin.getText());
        if(userLogInfo.get(1)!= null) {
            WindowSwitcher.getInstance().switchWindow(event, GuilURL.MAIN_WINDOW_URL, "Scooter Manager", true);
            ScootersHandler.setUserName(userLogInfo.get(2));
        }
    }

}
