package controller;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import model.GraphicUtil;
import model.GuilURL;
import model.Scooter;

import java.sql.SQLException;

public class MainController {

    @FXML
    private AnchorPane googleMapHolder;

    @FXML
    private ListView<Scooter> scooterListView;

    @FXML ListView<String> facettedSearchResult;

    @FXML
    private ImageView generalSearchIcon;

    @FXML
    private TextField scootersSearchField;

    @FXML
    private StackPane parentStackpaneLayout;

    @FXML
    private Button scootersSearchBtn, allScootersSearchBtn;


    public void initialize() throws SQLException, InterruptedException {
        setScooterHandler();
        setIcon();
        tune();
    }

    private  void setScooterHandler() throws SQLException, InterruptedException {
        final ScootersHandler scooterHandler = new ScootersHandler();
        scooterHandler
                .setParentStackpaneLayout(parentStackpaneLayout)
                .setGoogleMapHolder(googleMapHolder)
                .setScooterListView(scooterListView)
                .setScootersSearchBtn(scootersSearchBtn)
                .setAllScootersSearchBtn(allScootersSearchBtn)
                .buildModule();
    }
    private void setIcon(){
        GraphicUtil.getInstance().setIcon(generalSearchIcon, GuilURL.SEARCH_ICON);
    }

    private void tune(){
        allScootersSearchBtn.setAlignment(Pos.BASELINE_LEFT);
    }
}

