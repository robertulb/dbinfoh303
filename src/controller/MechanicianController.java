package controller;

import controller.dialog.DialogPaneFactory;
import controller.scootercell.ScooterCellFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import model.GuilURL;
import model.Scooter;
import model.User;
import model.db.MechanicianDAO;
import model.db.ScootersDAO;
import model.db.UserDAO;

import java.sql.SQLException;
import java.util.HashMap;

public class MechanicianController {

    @FXML
    private TextField scooterID;

    @FXML
    private  TextField scooterModel;

    @FXML
    private DatePicker scooterCommissioningDate;

    @FXML
    private Button addUserBtn;

    @FXML
   ListView<Scooter> allScooters;

    @FXML
    ListView<Scooter> scootersWithIssue;

    @FXML
    ListView<User> lambdaUsers;

    ContextMenu scooterContextMenu;

    @FXML
    StackPane mechanicMainView;


    private ObservableList<Scooter> allScooterList = FXCollections.observableArrayList();

    private ObservableList<Scooter> allScooterWithIssue = FXCollections.observableArrayList();

    private ObservableList<User> allUserList = FXCollections.observableArrayList();


    public  void initialize() throws SQLException {
        addNewScooter();
        setUpScooters(allScooters, allScooterList);
        setUpScooters(scootersWithIssue, allScooterWithIssue);
        changeUser();

    }

    private void changeUser() throws SQLException {
        // FIXME: 2019-05-13
        allUserList.addAll(UserDAO.getInstance().getAllAnonymeUsers());
        lambdaUsers.getItems().addAll(allUserList);
    }
    private void setUpScooters(ListView<Scooter> listView, ObservableList<Scooter> scooters) throws SQLException {
        // FIXME: 2019-05-13
        if(scooters==allScooterList)
            scooters.addAll(ScootersDAO.getInstance().readAvailableScooters());
        else
            scooters.addAll(ScootersDAO.getInstance().getScooterComplain());
        listView.getItems().addAll(scooters);
        listView.setCellFactory(param -> {
            ListCell<Scooter> scooterCell= new ScooterCellFactory();
            setScooterOptions(scooterCell);
            return  scooterCell;
        });
    }


    private void  addNewScooter() {
        addUserBtn.setOnAction(event -> {
            try {
                String scid= scooterID.getText().trim();
                String scm = scooterModel.getText().trim();
                if(scid!=null && scm!=null) {
                    MechanicianDAO.getInstance().insertScooter(scid, scooterCommissioningDate.getValue().toString(), scm);
                }
            } catch (SQLException e){

            }
        });

    }

    private void setScooterOptions(ListCell<Scooter> scooterCell) {
        scooterContextMenu= new ContextMenu();
        setContextMenu();
        scooterCell.emptyProperty().addListener((observable, oldValue, newValue) -> {
            scooterCell.setContextMenu(scooterContextMenu);
        });
    }

    private void setContextMenu() {
        MenuItem delete = new MenuItem("Delete");
        removeScooter(delete);
        MenuItem update = new MenuItem("update");
        updateScooter(update);
        scooterContextMenu.getItems().addAll(update, delete);
    }

    private void removeScooter(MenuItem menuItem){
        menuItem.setOnAction(event -> {
            try{
                Scooter scooter;
                if(allScooters.getSelectionModel().getSelectedItem()!=null) {
                    scooter = allScooters.getSelectionModel().getSelectedItem();
                    allScooters.getItems().remove(scooter);
            } else {
                    scooter = scootersWithIssue.getSelectionModel().getSelectedItem();
                    scootersWithIssue.getItems().remove(scooter);
                }
                MechanicianDAO.getInstance().deleteScooter(scooter.getScooterID().trim());
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }


    private void updateScooter(MenuItem menuItem){
        menuItem.setOnAction(event -> {
            try{
                Scooter scooter;
                if(allScooters.getSelectionModel().getSelectedItem()!=null) {
                    scooter = allScooters.getSelectionModel().getSelectedItem();
                    setDialogPane(GuilURL.MODIFYSCOOTER, "Change Scooter Status");
                } else {
                    scooter = scootersWithIssue.getSelectionModel().getSelectedItem();
                    setDialogPane(GuilURL.MODIFYSCOOTER, "Change Scooter Status");
                }
                MechanicianDAO.getInstance().deleteScooter(scooter.getScooterID().trim());
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }


    private void  setDialogPane(String fxmlUrl, String title) {
        HashMap<FXMLLoader, Dialog<ButtonType>> advancedSearchPane = DialogPaneFactory.getInstance().load(mechanicMainView,
                fxmlUrl, title);
        FXMLLoader fxmlLoader = null;
        for (FXMLLoader loader: advancedSearchPane.keySet()) {
            fxmlLoader = loader;
        }
        Dialog<ButtonType> dialog = advancedSearchPane.get(fxmlLoader);
        dialog.showAndWait();

    }




}
