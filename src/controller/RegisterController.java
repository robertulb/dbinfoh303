package controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import model.*;
import model.db.UserDAO;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.sql.SQLException;
public class RegisterController {

    /** first name of the user */
    @FXML
    private TextField firstName = null;
    /** last name of the user */
    @FXML
    private TextField lastName = null;
    /** useraccount of the user */
    @FXML
    private TextField useraccount = null;
    /** Date picker to allow the user to enter his birthday */

    @FXML
    private PasswordField password = null;

    @FXML
    private TextField userAddressStreet;

    @FXML
    private TextField userAddressNo;

    @FXML
    private TextField userAddressPostalCode;

    @FXML
    private TextField userAddressCity;

    @FXML
    private TextField userBankAccount;

    /** Text shown in case of error on the useraccount */
    @FXML
    private Text errorUseraccount = null;
    /** text shown in case of problem with the userbBankAcount */
    @FXML
    private Text errorBankAccount = null;
    /** confirmation of the password of the user */
    @FXML
    private PasswordField passwordConfirmation = null;
    /** terms condition that the user has to check to confirm it */
    @FXML
    private CheckBox usertypchoice = null;
    /** text shown in case of non check of the terms conditions */
    @FXML
    private Text errorTermsConfirmed = null;
    /** error shown in case of no password encoded */
    @FXML
    private Text errorPassword = null;
    @FXML
    private HBox userAddress;

    /**
     * Set the borders with no style to remove the red style if it was applied before
     * @param pCtrl Control on which new style must be applied
     */
    private void setNoneBorderStyle(Control pCtrl){
        pCtrl.setOnKeyReleased(keyEvent -> pCtrl.setStyle("-fx-border-color:none"));
    }

    /**
     * Initialise the choicebox
     */
    @FXML
    public void initialize() {
        setNoneBorderStyle(firstName);
        setNoneBorderStyle(lastName);
        setNoneBorderStyle(useraccount);
        setNoneBorderStyle(password);
        setNoneBorderStyle(passwordConfirmation);
        setNoneBorderStyle(userBankAccount);
        setFieldDisable();
        showMoreOption();
    }


    /**
     * Method to switch from a the register view to the login view and add a new user to the database.
     * @param event event that was triggered
     */
    @FXML
    public void registerAccount(ActionEvent event) throws IOException, SQLException {
        if(usertypchoice.isSelected()) {
            System.out.println("start registering power user");
            processPowerUserRegistration(event);
        } else {
            System.out.println("anonymous user");
            processAnonymousUserRegistration(event);
        }
    }


    private  void   processPowerUserRegistration(ActionEvent event) throws SQLException, IOException{
        System.out.println("going for field validation");
        if (!validatingChargerUserRegistration())return;
        RegisteredUser registeredUser = createRegistredUser();
        System.out.println("going to check wither user exists");
        if(!userExists(registeredUser.getID())){
            System.out.println("user seem not already exists");
            errorUseraccount.setVisible(false);
            addRegisteredUserToDB(registeredUser);
            switchToLoginWindow(event);
        }
        else {
            errorUseraccount.setVisible(true);
        }
    }


    private  void  processAnonymousUserRegistration(ActionEvent event) throws  SQLException, IOException {
        if(!validatingAnonymousUserRegistration()) return;
        User user = createAnonymousUser();
        if(!userExists(user.getID())) {
            addAnonymousUserToDB(user);
            switchToLoginWindow(event);
        }
    }


    /**
     * Add a given user to the database
     * @param registeredUser the user to register
     * @throws SQLException in case of malformed query
     */

    private void addRegisteredUserToDB(RegisteredUser registeredUser) throws SQLException {
        UserDAO.getInstance().createRegistredUser(registeredUser);
    }

    private  void addAnonymousUserToDB(User user) throws SQLException {
        UserDAO.getInstance().addAnonymousUser(user);
    }

/**
     * Check if the user exists in the database
     * @param user The user to check if the user is already registered
     * @return True if the user exists, false otherwise
     */

    private boolean userExists(String user) {
        System.out.println();
        try {
            String retrievedUser  = UserDAO.getInstance().readUserByUserID(user);
            System.out.println(retrievedUser);
            return retrievedUser != null;
        } catch (SQLException e) {
            new Alert(Alert.AlertType.ERROR, "The user does not exist").showAndWait();
            return false;
        }
    }


/**
     * method to create a user
     * @return a user after his creation
     */

    private RegisteredUser createRegistredUser() {
        String firstName = this.firstName.getText();
        String lastName = this.lastName.getText();
        String userAccount = this.useraccount.getText();
        String userPassword = this.password.getText();
        String bankAccount = this.userBankAccount.getText();
        String addressCity = userAddressCity.getText();
        String addressCodeP = userAddressPostalCode.getText();
        String addressNo = userAddressNo.getText();
        String addressStreet = userAddressStreet.getText();
        Address userAddress = new Address(addressCity, Integer.valueOf(addressCodeP), addressStreet, Integer.valueOf(addressNo));
        return new RegisteredUser(userAccount,userPassword,bankAccount, userAddress, firstName, lastName, "");
    }


    private  User createAnonymousUser() {
        return  new User(useraccount.getText(), password.getText(), userBankAccount.getText());
    }


    //validating anonymous user
    private boolean validatingAnonymousUserRegistration() {
        boolean everythingRight = true;
        if(!validatingUserAccount()) everythingRight = false;
        if(!validatingUserBankAccount()) everythingRight = false;
        if(!validationPasswords()) everythingRight = false;
        return  everythingRight;
    }



/**
     * Method to switch from a the register view to the login view without registering the user.
     * @param event Event that was triggered
     */

    @FXML
    private void switchToLoginWindow(ActionEvent event) throws IOException {
        WindowSwitcher.getInstance().switchWindow(event,  GuilURL.LOGIN_URL, "Login",false);
    }


    /**
     * check all the mandatory fields and validate it
     * @return true if everything is ok otherwise false.
     * @throws SQLException in case of malformed query
     */
    private boolean validatingChargerUserRegistration() {
        boolean everythingRight = true;
        if(!validatingTextField(firstName)) everythingRight = false;
        System.out.println("validation result1 is: " + everythingRight);
        if(!validatingTextField(lastName)) everythingRight = false;
        System.out.println("validation result2 is: " + everythingRight);
        if(!validatingTextField(userAddressCity)) everythingRight =false;
        System.out.println("validation result3 is: " + everythingRight);
        if(!validatingTextField(userAddressStreet)) everythingRight = false;
        System.out.println("validation result4 is: " + everythingRight);
        if(!validatingUserAddressNo()) everythingRight =false;
        System.out.println("validation result5 is: " + everythingRight);
        if(!validatingUserAddressPostalCode()) everythingRight =false;
        System.out.println("validation result6 is: " + everythingRight);
        if(!validatingUserAccount()) everythingRight = false;
        System.out.println("validation result7 is: " + everythingRight);
        if(!validatingUserBankAccount()) everythingRight = false;
        System.out.println("validation result8 is: " + everythingRight);
        if(!validationPasswords()) everythingRight = false;
        System.out.println("validation result is: " + everythingRight);
        return  everythingRight;

    }


    private boolean validatingUserAddressNo() {
        if(validatingTextField(userAddressNo)){
            if(StringUtils.isNumeric(userAddressNo.getText())) {
                return true;
            }
        }
        System.out.println("address no fail");
        return false;
    }


    private boolean validatingUserAddressPostalCode() {
        String cp =  userAddressPostalCode.getText();
        if(validatingTextField(userAddressPostalCode)){
            if(StringUtils.isNumeric(cp) && cp.length() ==4) return true;
        }
        return false;
    }

    private boolean validatingUserBankAccount() {
        if(validatingTextField(userBankAccount)){
            String bankAccount = userBankAccount.getText();
            if(StringUtils.isNumeric(bankAccount) && bankAccount.length() ==16){
                errorBankAccount.setText("Bank account is not valid");
                return true;
            }
        }
        System.out.println("Bank account has issue");
        return false;
    }

    /**
     * Set to red if the textfield is empty and should be filled
     * @param field Textfield to validate
     * @return true if it is filled
     */
    private boolean validatingTextField(TextField field) {
        if(field.getText().isEmpty()){
            field.setStyle("-fx-border-color:#FA5858");
            return false;
        }
        return true;
    }

    /**
     * Check if the useraccount field is filled
     * @return true if the useraccount field is filled. False otherwise
     */
    private boolean validatingUserAccount(){
        if(validatingTextField(useraccount)){
            if(StringUtils.isNumeric(useraccount.getText())){
                System.out.println("it's a number");
                return true;
            }
        }
        errorUseraccount.setVisible(true);
        System.out.println("user account has issue");
        return false;
    }



    /**
     * Check if the password is the same in password and confirmation password.
     * @return true if the password is correct and false if it is not.
     */
    private boolean validationPasswords(){
        validatingTextField(passwordConfirmation);
        validatingTextField(password);
        if(validatingTextField(passwordConfirmation) && validatingTextField(password)){
            if(password.getText().equals(passwordConfirmation.getText())){
                errorPassword.setVisible(false);
                return true;
            }
            errorPassword.setVisible(true);
        }
        return false;

    }

    /**
     * Check of the terms are check
     * @return if checked true, if not, return false and display an error.
     */
    private void showMoreOption(){
        usertypchoice.selectedProperty().addListener((observable, oldValue, newValue) -> {
           if(usertypchoice.isSelected()){
               setFieldEnable();
           } else {
               setFieldDisable();
           }
        });
    }

   private void setFieldDisable() {
       firstName.setDisable(true);
       lastName.setDisable(true);
       userAddress.setDisable(true);
   }

    private void setFieldEnable() {
        firstName.setDisable(false);
        lastName.setDisable(false);
        userAddress.setDisable(false);
    }

    /**
     * Set invisible the terms conditions
     */
    @FXML
    public void resetCheckBox(){
        errorTermsConfirmed.setVisible(false);
    }

}
