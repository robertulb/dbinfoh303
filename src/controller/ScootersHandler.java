package controller;
import controller.dialog.DialogPaneFactory;
import controller.scootercell.ScooterCellFactory;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import model.GuilURL;
import model.Location;
import model.Scooter;
import model.db.ScootersDAO;
import model.db.UserDAO;
import java.sql.SQLException;
import java.util.HashMap;
public class ScootersHandler {

    private static ListView<Scooter> scooterListView;

    private Button scootersSearchBtn, allScootersSearchBtn;

    private  GoogleMapHandler mapHandler;

    private ObservableList<Scooter> scooters;

    private ContextMenu contextMenu;

    private static  String userName;

    private StackPane parentStackpaneLayout;

    public ScootersHandler()  {
    }

    public ScootersHandler setGoogleMapHolder(AnchorPane googleMapHolder) {
        this.mapHandler =  new GoogleMapHandler(googleMapHolder);
        mapHandler.setMapView();
        return this;
    }

    public ScootersHandler setScooterListView(ListView<Scooter> scooterListView) {
        this.scooterListView = scooterListView;
        return  this;
    }

    public ScootersHandler setScootersSearchBtn(Button scootersSearchBtn) {
        this.scootersSearchBtn = scootersSearchBtn;
        return  this;
    }

    public ScootersHandler setAllScootersSearchBtn(Button allScootersSearchBtn) {
        this.allScootersSearchBtn = allScootersSearchBtn;
        return this;
    }

    public ScootersHandler setParentStackpaneLayout(StackPane parentStackpaneLayout) {
        this.parentStackpaneLayout = parentStackpaneLayout;
        return this;
    }

    public static  void setUserName(String userName) {
        ScootersHandler.userName = userName;
    }

    public void buildModule() throws  SQLException {
        setScooterView();
        this.scooters = FXCollections.observableArrayList(ScootersDAO.getInstance().readAvailableScooters());
        GoogleMapHandler.setScooters(scooters);
    }


    private void setScooterView() {
        allScootersSearchBtn.setOnAction(event -> {
            Platform.runLater(() -> {
                scooterListView.getItems().setAll(scooters);
            });
        });
        scooterListView.setCellFactory(param -> {
            ListCell<Scooter> scooterCell = new ScooterCellFactory();
            setScooterOptions(scooterCell);
            return  scooterCell;
        });

    }

    private void setScooterOptions(ListCell<Scooter> scooterCell) {
        setContextMenu();
        scooterCell.emptyProperty().addListener((observable, oldValue, newValue) -> {
            scooterCell.setContextMenu(contextMenu);
        });
    }

    private void setContextMenu() {
        contextMenu = new ContextMenu();
        MenuItem tripsHistoric = new MenuItem("Views Trips Historic");
        tripsHistoric.setOnAction(event -> viewScooterTripsHistoric());
        MenuItem complain = new MenuItem("Make A Complaint");
        complain.setOnAction(event -> {
            try {
                makeComplaint();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
        if(userName!=null){
            MenuItem chargeItem = new MenuItem("Make A Recharge");
            chargeItem.setOnAction(event -> chargeScooter());
            contextMenu.getItems().addAll(tripsHistoric, complain, chargeItem);
        } else {
            contextMenu.getItems().addAll(tripsHistoric, complain);
        }
    }

    private void chargeScooter() {
        setDialogPane( GuilURL.CHARGING_URL,  "Charging Form");
    }


    private  void viewScooterTripsHistoric() {
        setDialogPane(GuilURL.TRIPS_HISTORIC_URL, "Scooter ID"+
                scooterListView.getSelectionModel().getSelectedItem().getScooterID() + " Trips Historic");
    }

    private  void makeComplaint() throws SQLException {
        UserDAO.getInstance().submitComplain(scooterListView.getSelectionModel().getSelectedItem().getScooterID().trim(), true);

        new Alert(Alert.AlertType.INFORMATION, "Complaint has been set").showAndWait();
    }

    private FXMLLoader setDialogPane(String fxmlUrl, String title) {
        HashMap<FXMLLoader, Dialog<ButtonType>> advancedSearchPane = DialogPaneFactory.getInstance().load(parentStackpaneLayout,
                fxmlUrl, title);
        FXMLLoader fxmlLoader = null;
        for (FXMLLoader loader: advancedSearchPane.keySet()) {
            fxmlLoader = loader;
        }
        Dialog<ButtonType> dialog = advancedSearchPane.get(fxmlLoader);
        dialog.showAndWait();
        return fxmlLoader;
    }

    public  static Scooter   getSelectedItem(){
        return scooterListView.getSelectionModel().getSelectedItem();
    }

}
