package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.Objects;

/**
 * This class allows us to switch the windows from one view to another
 */
class WindowSwitcher {

    private static WindowSwitcher ourInstance = new WindowSwitcher();

    /**
     * @return the instance of the {@link #WindowSwitcher}
     */
    static WindowSwitcher getInstance() {
        return ourInstance;
    }

    private WindowSwitcher() { }

    /**
     * This method allows us to set the parameters of the UI and center the window on the user screen.
     * @param event The event that will be raised by the window.
     * @param fxmlResource FXML file which define the style of the window.
     * @param stageTitle Title of the current window display (Ex: Login, Register ...).
     * @param isWindowMax Set the value for the size of the window.
     * @throws IOException this exception is throw for the FXMLLoader
     */
    void switchWindow(ActionEvent event, String fxmlResource, String stageTitle, boolean isWindowMax) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(getClass().getClassLoader().getResource(fxmlResource)));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setResizable(isWindowMax);
        stage.setMaximized(false);
        stage.setScene(scene);
        stage.setTitle(stageTitle);
        stage.centerOnScreen();
        stage.show();
    }
}