package controller.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

import java.io.IOException;
import java.util.HashMap;


/**
 * @author yveskerbensdeclerus
 * Controller factory for dialogbox
 */
public class DialogPaneFactory {

    /**
     * Instantiation of the DialogPaneFactory
     */
    private static DialogPaneFactory instance = new DialogPaneFactory();

    private DialogPaneFactory() { }

    /**
     * Retrieve the instance of the {@link #DialogPaneFactory()}
     * @return the instance of the object
     */
    public static DialogPaneFactory getInstance() {
        return instance;
    }

    /**
     *
     * @param owner the owner node from which the dialog pane will be called
     * @param fxmlUrl the url of the fxml ressource
     * @param title the title of the dialog pane
     * @return map of {@link FXMLLoader} and dialog pane
     */
    public HashMap<FXMLLoader, Dialog<ButtonType>> load(Node owner, String fxmlUrl, String title) {
        HashMap<FXMLLoader, Dialog<ButtonType>> dialogPaneMap = new HashMap<>();
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(owner.getScene().getWindow());
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource(fxmlUrl));
        try{
            dialog.getDialogPane().setContent(fxmlLoader.load());
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, "An error occurred during the display of page resource").showAndWait();
        }
        if(title!=null) {
            dialog.setTitle(title);
        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        dialogPaneMap.put(fxmlLoader, dialog);
        return dialogPaneMap;
    }
}
