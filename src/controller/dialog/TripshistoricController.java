package controller.dialog;

import controller.ScootersHandler;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Scooter;
import model.Trip;
import model.db.TripsDAO;

import java.sql.SQLException;


public class TripshistoricController {
    @FXML
    private TableView tripHistoricTable;

    public  void initialize() {
        TableColumn userCol = new TableColumn("User ID");
        userCol.setCellValueFactory(new PropertyValueFactory<Trip, String>("userid"));

        TableColumn startTimeCol = new TableColumn("Start Time");
        startTimeCol.setCellValueFactory(new PropertyValueFactory<Trip, String>("starttime"));

        TableColumn endTimeCol = new TableColumn("End Time");
        endTimeCol.setCellValueFactory(new PropertyValueFactory<Trip, String>("endtime"));

        TableColumn distanceCol = new TableColumn("Distance(km)");
        distanceCol.setCellValueFactory(new PropertyValueFactory<Trip, Float>("distance"));

        userCol.setPrefWidth(130);
        startTimeCol.setPrefWidth(170);
        endTimeCol.setPrefWidth(170);
        distanceCol.setPrefWidth(130);
        tripHistoricTable.getColumns().addAll(userCol, startTimeCol, endTimeCol, distanceCol);

        try{
            Scooter scooter = ScootersHandler.getSelectedItem();
            ObservableList<Trip> trips = FXCollections.observableArrayList(TripsDAO.getInstance().scooterDeplacementHistory(scooter.getScooterID().trim()));
            tripHistoricTable.setItems(trips);

        }catch (SQLException e) {
            e.printStackTrace();

        }
    }
}
