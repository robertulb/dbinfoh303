package controller.scootercell;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;


/**
 * This class handle the loading of a FXML file resource for widgets
 */
public abstract class AbstractController<Y> extends HBox {

    /**
     * Load a FXML file to change the interface
     * @param fxmlFile The FXML file who will be loaded to the user window
     * @return the fxmlLoader
     */
    protected FXMLLoader getLoaderFxml(String fxmlFile) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(fxmlFile));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);
        return fxmlLoader;
    }

    public  abstract  Y getNode();
}
