package controller.scootercell;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import model.GuilURL;
import model.Scooter;

import java.io.IOException;

/**
 * This class allow us to feed the right part of
 * the panel with different RSS flows.
 */
public  class ScooterCellController extends AbstractController<HBox> {

    private Scooter scooter;

    @FXML
    private Label scooterID = null;

    @FXML
    private  Label scooterModel = null;

    @FXML
    private Button batterLevel = null;

    @FXML
    private Button availability = null;


    @FXML
    private  HBox scooterCellHbox = null;

    public ScooterCellController() throws IOException{
        super.getLoaderFxml(GuilURL.CHANNEL_SCOOTER_CELL).load();
    }


    public  void setInfo(Scooter scooter) {
        this.scooter = scooter;
        this.scooterID.setText("Scooter ID: " + scooter.getScooterID());
        this.scooterModel.setText("Scooter Model: " + scooter.getModel());
        setBatterStatus(scooter);
        setAvailabilityStatus(scooter);
    }

    private void setBatterStatus(Scooter scooter){
        switch (scooter.getBatterLevel()){
            case 1:
                batterLevel.setStyle("-fx-background-color: red");
                break;


            case 2:
                batterLevel.setStyle("-fx-background-color: orange");
                break;

            case 3:
                batterLevel.setStyle("-fx-background-color: yellow");
                break;

            default:
                batterLevel.setStyle("-fx-background-color: green");
                break;

        }
    }

    private void setAvailabilityStatus(Scooter scooter) {
        if(scooter.isAvailable()){
            availability.setStyle("-fx-background-color: green");
        } else {
            availability.setStyle("-fx-background-color: red");
        }
    }

    public HBox getNode() {
        return  scooterCellHbox;
    }

}
