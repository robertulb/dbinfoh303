package controller.scootercell;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import model.Scooter;

import java.io.IOException;

/**
 * @author yveskerbensdeclerus
 * Creating  list view's customized cell.
 */

//Fixme: Need to be implemented either tomorrow or next iteration
public class ScooterCellFactory extends ListCell<Scooter>{



    @Override
    protected void updateItem(Scooter scooter, boolean empty){
        super.updateItem(scooter, empty);
        if(empty) {
            setGraphic(null);
        } else {
            setGraphic(setUpDefaultCell(scooter));
        }
    }
    private HBox setUpDefaultCell(Scooter scooter)  {
        try { ScooterCellController controller = new ScooterCellController();
            controller.setInfo(scooter);
            return  controller.getNode();
        } catch (IOException e){
            return null;
        }
    }


}
