package model;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateUtil {
    private static DateUtil ourInstance = new DateUtil();
    private final SimpleDateFormat formatter;

    public static DateUtil getInstance() {
        return ourInstance;
    }

    private DateUtil() {
        formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    }

    public Timestamp getdate(String dateString) {
        try {
            return  new Timestamp(formatter.parse(dateString).getTime());
        } catch (ParseException e) {
            return  null;
        }
    }
}
