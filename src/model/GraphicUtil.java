package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.Objects;

/**
 * @author yveskerbensdeclerus
 * utility to tools to set graphic item to user iterface
 */
public class GraphicUtil {
    private static GraphicUtil ourInstance = new GraphicUtil();

    /**
     * @return Singleton of {@link GraphicUtil}
     */
    public static GraphicUtil getInstance() {
        return ourInstance;
    }

    private GraphicUtil() {}

    /**
     *
     * @param imageViewNode The {@link ImageView} element of the fxml file
     * @param iconURL the url constant  of the resource to be setup as image
     */
    public void setIcon(ImageView imageViewNode, String iconURL) {
        Image image = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(iconURL)).toExternalForm()) ;
        imageViewNode.setImage(image);
    }

}
