package model;

public class GuilURL {
    public static final String REGISTER_URL = "view/GUI/register.fxml";
    public static final String LOGIN_URL =  "view/GUI/login.fxml";
    public static final String MAIN_WINDOW_URL =  "view/GUI/main.fxml";
    public static final String CHANNEL_SCOOTER_CELL = "view/GUI/scooters.fxml";
    public static final String TRIPS_HISTORIC_URL = "view/GUI/tripshistoric.fxml" ;
    public static final String CHARGING_URL = "view/GUI/charging.fxml" ;
    public static final String MODIFYSCOOTER = "view/GUI/modifyscooter.fxml";
    private static final String PREFIX = "view/";
    public static final String SEARCH_ICON = PREFIX + "image/searchIcon.png";
    public static final String MACHANICIAN_URL = "view/GUI/mechanicians.fxml";
}
