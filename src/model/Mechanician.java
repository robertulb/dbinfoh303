package model;


public class Mechanician extends  Person{
    private String hireDate;

    public Mechanician(String ID, String password, String bankaccount, String hireDate, Address address,
                       String firstName, String lastName, String phone) {
        super(ID, password, bankaccount, address, firstName, lastName, phone);
        this.hireDate = hireDate;
    }

    public String getID() {
        return super.getID();
    }

    public String getPassword() {
        return super.getPassword();
    }

    public String getBankaccount() {
        return super.getBankaccount();
    }

    public Address getAddress() {
        return super.getAddress();
    }

    public String getFirstName() {
        return super.getFirstName();
    }

    public String getLastName() {
        return super.getLastName();
    }

    public String getPhone() {
        return super.getPhone();
    }

    public String getHireDate() {
        return hireDate;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + hireDate;
    }
}
