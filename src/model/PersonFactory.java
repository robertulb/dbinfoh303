package model;

import org.jdom.Element;

import java.util.ArrayList;
import java.util.List;

public class PersonFactory {
    private static PersonFactory ourInstance = new PersonFactory();

    public static PersonFactory getInstance() {
        return ourInstance;
    }

    private PersonFactory() {
    }

    public   List<Person> build(String typeOfPerson, List<Element> elements){
        List<Person> people = new ArrayList<>();
            for(int i=0; i<=elements.size()-1; i++) {
                Element personEl = elements.get(i);
                if(typeOfPerson.equals("registeredUser")) {
                    RegisteredUser registeredUser = new RegisteredUser(personEl.getChild("ID").getValue(), personEl.getChild("password").getValue(),
                        personEl.getChild("bankaccount").getValue(), getAddress(personEl), personEl.getChild("firstname").getValue(),
                        personEl.getChild("lastname").getValue(), personEl.getChild("phone").getValue());
                    people.add(registeredUser);
                } else if(typeOfPerson.equals("mechanician")){
                    Mechanician mechanician = new Mechanician(personEl.getChild("mechanicID").getValue(), personEl.getChild("password").getValue(),
                            personEl.getChild("bankaccount").getValue(),  personEl.getChild("hireDate").getValue(),
                            getAddress(personEl), personEl.getChild("firstname").getValue(),
                            personEl.getChild("lastname").getValue(), personEl.getChild("phone").getValue());
                   people.add(mechanician);
                }

        }
        return people;
    }

    private Address getAddress(Element element) {
        Element addressE = element.getChild("address");
        return  new Address(addressE.getChild("city").getValue(),
                Integer.parseInt(addressE.getChild("cp").getValue()),
                addressE.getChild("street").getValue(), Integer.parseInt(addressE.getChild("number").getValue()));
    }
}
