package model;

public class RegisteredUser extends  Person{
    public RegisteredUser(String ID, String password, String bankaccount,
                          Address address, String firstName, String lastName, String phone) {
        super(ID, password, bankaccount, address, firstName, lastName, phone);
    }

    public String getID() {
        return super.getID();
    }

    public String getPassword() {
        return super.getPassword();
    }

    public String getBankaccount() {
        return super.getBankaccount();
    }

    public Address getAddress() {
        return super.getAddress();
    }

    public String getFirstName() {
        return super.getFirstName();
    }

    public String getLastName() {
        return super.getLastName();
    }

    public String getPhone() {
        return super.getPhone();
    }
}
