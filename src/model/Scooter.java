package model;

public class Scooter {
    private String scooterID;
    private String model;
    private String commissioningDate;
    private boolean availability;
    private int batterLevel;
    private boolean complain;
    private Location position;

    public Scooter(String scooterID, String model, String commissioningDate,
                  int batterLevel,  boolean availability,  boolean complain, Location location) {
        this.scooterID = scooterID;
        this.model = model;
        this.commissioningDate = commissioningDate;
        this.availability = availability;
        this.batterLevel = batterLevel;
        this.complain = complain;
    }

    public String getScooterID() {
        return scooterID;
    }

    public String getModel() {
        return model;
    }

    public String getCommissioningDate() {
        return commissioningDate;
    }

    public boolean isAvailability() {
        return availability;
    }

    public int getBatterLevel() {
        return batterLevel;
    }

    public boolean isComplain() {
        return complain;
    }

    public Location getPosition() {
        return position;
    }

    public boolean isAvailable() {
        return availability;
    }

    public void setPosition(Location position) {
        this.position = position;
    }
}
