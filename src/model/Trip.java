package model;

public class Trip {

    private float distance;
    private String scooterid;
    private String userid;
    private String starttime;
    private String endtime;

    public Trip(String scooterid, String userid, float distance, String starttime, String endtime) {

        this.scooterid = scooterid;
        this.userid = userid;
        this.distance = distance;
        this.starttime = starttime;
        this.endtime = endtime;
    }

    public String getScooterid() {
        return scooterid;
    }

    public String getUserid() {
        return userid;
    }

    public String getStarttime() {
        return starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public double getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Trip  for user whose ID is " + userid + ": " + distance + " "+ starttime + "  " + endtime;
    }
}
