package model.db;

import model.Address;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddressDAO {
    private static AddressDAO ourInstance = new AddressDAO();

    public static AddressDAO getInstance() {
        return ourInstance;
    }

    private AddressDAO() {
    }


    public void insertAddress(String id, Address address) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryFactory.getInstance().getInsertAddress());
        preparedStatement.setString(1, id);
        preparedStatement.setString(2, address.getCity());
        preparedStatement.setInt(3, address.getCp());
        preparedStatement.setString(4, address.getStreet());
        preparedStatement.setInt(5, address.getNumber());
        preparedStatement.executeUpdate();
    }

}
