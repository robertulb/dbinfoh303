package model.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBConnection {

    private static DBConnection instance = new DBConnection();

    private Connection connection;

    private PreparedStatement preparedStatement;

    public static DBConnection getInstance(){
        return instance;
    }

    String url = "jdbc:postgresql://localhost:5432/" +DBConstants.SCOOTERS_DB;

    private DBConnection() {
        try {
            connection = DriverManager.getConnection(url, "yveskerbensdeclerus", "admin");
           /* createTable(SQLQueryFactory.getInstance().getCreateTripsTableQuery());
            createTable(SQLQueryFactory.getInstance().getCreateScootersTalbleQuery());
            //System.out.println(SQLQueryFactory.getInstance().getCreateUserTableQuery());
            createTable(SQLQueryFactory.getInstance().getCreateUserTableQuery());
            createTable(SQLQueryFactory.getInstance().getCreateMechanicianTableQuery());
            createTable(SQLQueryFactory.getInstance().getCreateAdressTableQuery());
            createTable(SQLQueryFactory.getInstance().getInsertRegisteredUserTableQuery());*/
        } catch (SQLException e) {
            System.out.println("Fail to connect to DB" + e.getMessage());
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    private void createTable(String query)  {
        try {
            preparedStatement= connection.prepareStatement(query);
            preparedStatement.execute();
        } catch (SQLException e) {
            System.out.println("Fail to connect to DB" + e.getMessage());
        }
    }

}
