package model.db;

public class DBConstants {
    public static final String SCOOTERS_DB = "scooters";

    //Table trips
    public static final String TRIPS_TABLE = "infoh303.trips";
    public static final String SCOOTER_TRIP_SCOOTER_ID= "scooterid";
    public static final String SCOOTER_TRIP_USER_ID= "userid";
    public static final  String SCOOTER_TRIP_SOURCEX= "sourcex";
    public static final  String SCOOTER_TRIP_SOURCEY= "sourcey";
    public static final  String SCOOTER_TRIP_DESTINATIONX= "destinationx";
    public static final  String SCOOTER_TRIP_DESTINATIONY= "destinationy";
    public static final  String SCOOTER_TRIP_STARTTIME= "starttime";
    public static final  String SCOOTER_TRIP_ENDTIME= " endtime";

    //Table scooters
    public static final String SCOOTER_TABLE = "infoh303.scooters";
    public static final String SCOOTER_SCOOTER_ID = "scooterid";
    public static final String SCOOTER_SCOOTER_COMMISSIONING_DATE = "commissioning_date";
    public static final String SCOOTER_SCOOTER_MODEL = "model";
    public static final String SCOOTER_SCOOTER_COMPLAINT = "complain";
    public static final String SCOOTER_SCOOTER_BATTERLEVEL = "battery_level";
    public static final String SCOOTER_SCOOTER_AVAILABILITY = "availability";

    //Table reparations
    public static final String REPARATION_TABLE = "infoh303.reparations";
    public static final String REPARATION_SCOOTER = "scooterID";
    public static final String REPARATION_USER = "userID";
    public static final String REPARATION_MECHANIC = "mechanicID";
    public static final String REPARATION_COMPLAINTIME = "complaintime";
    public static final String REPARATION_REPAIRTIME = "repairtime";
    public static final String REPARATION_NOTE = "note";

    //Table Reloads
    public static final String RELOADS_TABLE = "infoh303.reloads";
    public static final String RELOADS_SCOOTER= "scooterid";
    public static final String RELOADS_USER = "userid";
    public static final String RELOADS_INITIALLOAD = "initialload";
    public static final String RELOADS_FINALLOAD = "finalload";
    public static final String RELOADS_SOURCEX = "sourcex";
    public static final String RELOADS_SOURCEY = "sourcey";
    public static final String RELOADS_DESTINATIONX = "destinationx";
    public static final String RELOADS_DESTINATIONY = "destinationy";
    public static final String RELOADS_STARTTIME= "starttime";
    public static final String RELOADS_ENDTIME = "endtime";

    //Table all_users
    public static final String USERS_TABLE = "infoh303.allusers";
    public static final String USERS_ID = "userID";
    public static final String USERS_PASSWORD ="password";
    public static final String USERS_BANKACCOUNT = "bankaccount";

    // Table Registered User
    public static final String REGISTERED_USER_TABLE = "infoh303.registeredusers";
    public static final String REGISTERED_USER_ID =  "userID";
    public static final String REGISTERED_USER_FIRSTNAME= "firstname";
    public static final String REGISTERED_USER_LASTNAME = "lastname";
    public static final String REGISTERED_USER_PHONE = "phone";

    //Table Mechanician
    public static final String MECHANICIAN_TABLE = "infoh303.mechanic";
    public static final String MECHANICIAN_ID = "mechanicID";
    public static final String MECHANICIAN_LASTNAME = "lastname";
    public static final String MECHANICIAN_FIRSTNAME = "firstname";
    public static final String MECHANICIAN_PASSWORD ="password";
    public static final String MECHANICIAN_PHONE ="phone";
    public static final String MECHANICIAN_HIREDATE ="hireDate";
    public static final String MECHANICIAN_BANKACCOUNT = "bankaccount";

    //Table adresse
    public static final String ADDRESSE_TABLE = "infoh303.address";
    public static final String ADDRESSE_PERSONID = "personID";
    public static final String ADDRESSE_CITY = "city";
    public static final String ADDRESSE_CP = "cp";
    public static final String ADDRESSE_STREET = "street";
    public static final String ADDRESSE_NUMBER = "number";
}