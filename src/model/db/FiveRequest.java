package model.db;

import model.Scooter;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FiveRequest {
    private static FiveRequest ourInstance = new FiveRequest();

    public static FiveRequest getInstance() {
        return ourInstance;
    }

    private FiveRequest() {
    }

    //1
    public List<Scooter> readAvailableScootersFiveQuery() throws SQLException {
        return ScootersDAO.getInstance().readAvailableScooters();
    }

    //4
    public List<String> scooterWithMore4complain() throws SQLException {
        List<String> scooterIds = new ArrayList<>();
        String query = "select scooters.scooterid " +
                        "from infoh303.reparations, infoh303.scooters " +
                        "where reparations.scooterid = scooters.scooterid " +
                        "group by scooters.scooterid " +
                        "having count(reparations.complaintime)>10";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();

        while (resultSet.next()) {
            String scooter = resultSet.getString(1);
            scooterIds.add(scooter);
        }
        return scooterIds;
    }

    private List<String> getTenTripsUsersInTrip() throws SQLException {
        String query = "select trips.userid, count(trips.userid) " +
                        "from infoh303.trips " +
                        "group by trips.userid " +
                        "having count(trips.userid)>10;";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();
        List<String> userid = new ArrayList<>();

        while (resultSet.next()) {
            userid.add(resultSet.getString(1));
        }
        return userid;
    }

    //5
    public List<User> getTenTripsUser() throws SQLException {
        List<User> usersData = new ArrayList<>();
        Connection connection = DBConnection.getInstance().getConnection();

        for (String userid : getTenTripsUsersInTrip()) {

            String query = "select * from infoh303.allusers " +
                    "where userid = ?";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, userid);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                User user = new User(resultSet.getString(1),
                                    resultSet.getString(2),
                                    resultSet.getString(3));

                usersData.add(user);
            }
        }
        return usersData;
    }

}
