package model.db;

import model.DateUtil;
import model.Mechanician;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MechanicianDAO {

    private static MechanicianDAO ourInstance = new MechanicianDAO();

    public static MechanicianDAO getInstance() {
        return ourInstance;
    }

    private MechanicianDAO() {
    }

    public  void insertMechanicians(List<Mechanician> mechanicians) throws SQLException {
        for(Mechanician mechanician: mechanicians) {
            insertMecanicianGeneralInfo(mechanician);
            AddressDAO.getInstance().insertAddress(mechanician.getID(), mechanician.getAddress());
        }
    }

    private void insertMecanicianGeneralInfo(Mechanician mechanician) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryFactory.getInstance().getInsertMechanicianQuery());
        preparedStatement.setString(1, mechanician.getID());
        preparedStatement.setString(2, mechanician.getFirstName());
        preparedStatement.setString(3, mechanician.getLastName());
        preparedStatement.setString(4,  mechanician.getPassword());
        preparedStatement.setString(5, mechanician.getPhone());
        preparedStatement.setTimestamp(6, DateUtil.getInstance().getdate(mechanician.getHireDate()));
        preparedStatement.setString(7, mechanician.getBankaccount());

        preparedStatement.executeUpdate();
    }

}
