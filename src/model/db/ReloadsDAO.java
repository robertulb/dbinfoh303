package model.db;

import model.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ReloadsDAO {
    private static ReloadsDAO ourInstance = new ReloadsDAO();

    public static ReloadsDAO getInstance() {
        return ourInstance;
    }

    private ReloadsDAO() {
    }

    public void insertReloadsDAO(List<String[]> reloads) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
                SQLQueryFactory.getInstance().getInsertReloadsQuery());
        reloads.remove(0);
        for (String[] reload: reloads) {
            preparedStatement.setString(1, String.valueOf(reload[0]));
            preparedStatement.setString(2, String.valueOf(reload[1]));
            preparedStatement.setInt(3, Integer.parseInt(reload[2]));
            preparedStatement.setInt(4, Integer.parseInt(reload[3]));
            preparedStatement.setDouble(5, Double.parseDouble(reload[4]));
            preparedStatement.setDouble(6, Double.parseDouble(reload[5]));
            preparedStatement.setDouble(7, Double.parseDouble(reload[6]));
            preparedStatement.setDouble(8, Double.parseDouble(reload[7]));
            preparedStatement.setTimestamp(9, DateUtil.getInstance().getdate(reload[8]));
            preparedStatement.setTimestamp(10, DateUtil.getInstance().getdate(reload[9]));
            preparedStatement.executeUpdate();
        }
    }


}
