package model.db;

import model.DateUtil;
import model.Scooter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReparationsDAO {
    private static ReparationsDAO ourInstance = new ReparationsDAO();

    public static ReparationsDAO getInstance() {
        return ourInstance;
    }

    private ReparationsDAO() {
    }

    public void insertReparationsDAO(List<String[]> reparations) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(
                SQLQueryFactory.getInstance().getInsertRepairQuery());
        reparations.remove(0);

        for (String[] reparation: reparations) {
            preparedStatement.setString(1, String.valueOf(reparation[0]));
            preparedStatement.setString(2, String.valueOf(reparation[1]));
            preparedStatement.setString(3, String.valueOf(reparation[2]));
            preparedStatement.setTimestamp(4, DateUtil.getInstance().getdate(reparation[3]));
            preparedStatement.setTimestamp(5, DateUtil.getInstance().getdate(reparation[4]));
            preparedStatement.setString(6, String.valueOf(" "));
            preparedStatement.executeUpdate();
        }
    }

    public void updateComplain (String scooterid, String note, String date) throws SQLException {
        //todo vérifier repair time
        String query = "update infoh303.reparations " +
                "set note = ?  " +
                "where scooterid =? and repairtime =?;";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, note);
        preparedStatement.setString(2, scooterid);
        preparedStatement.setTimestamp(3, DateUtil.getInstance().getdate(date));
        //affectedrows = preparedStatement.executeUpdate();
        preparedStatement.executeUpdate();
    }

}
