package model.db;

public class SQLQueryFactory {
    private static SQLQueryFactory ourInstance = new SQLQueryFactory();

    public static SQLQueryFactory getInstance() {
        return ourInstance;
    }

    private SQLQueryFactory() {

    }

    public String getCreateUserTableQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.USERS_TABLE);
        sb.append("(");
        sb.append(DBConstants.USERS_ID);
        sb.append(" varchar (15), ");
        sb.append(DBConstants.USERS_PASSWORD);
        sb.append(" varchar(30) NOT NULL, ");
        sb.append(DBConstants.USERS_BANKACCOUNT);
        sb.append(" varchar(30), ");
        sb.append("primary key  (");
        sb.append(DBConstants.USERS_ID);
        sb.append("))");
        //System.out.println(sb);
        return sb.toString();
    }

    public String getInsertAnUsersQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.USERS_TABLE);
        sb.append("(");
        sb.append(DBConstants.USERS_ID);
        sb.append(", ");
        sb.append(DBConstants.USERS_PASSWORD);
        sb.append(", ");
        sb.append(DBConstants.USERS_BANKACCOUNT);
        sb.append(") VALUES (?, ?, ?)");
        System.out.println(sb);
        return sb.toString();
    }

    public String getCreateScootersTalbleQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.SCOOTER_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_SCOOTER_ID);
        sb.append(" char(25) NOT NULL, ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMMISSIONING_DATE);
        sb.append(" timestamptz NOT NULL, ");
        sb.append(DBConstants.SCOOTER_SCOOTER_MODEL);
        sb.append(" char(12), ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMPLAINT);
        sb.append("  boolean, ");
        sb.append(DBConstants.SCOOTER_SCOOTER_BATTERLEVEL);
        sb.append(" char(25), ");
        sb.append(DBConstants.SCOOTER_SCOOTER_AVAILABILITY);
        sb.append(" boolean, ");
        sb.append("primary key  (");
        sb.append(DBConstants.SCOOTER_SCOOTER_ID);
        sb.append("))");
        return sb.toString();
    }

    public String getInsertScooterQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.SCOOTER_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_SCOOTER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMMISSIONING_DATE);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_MODEL);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMPLAINT);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_BATTERLEVEL);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_AVAILABILITY);
        sb.append(") VALUES (?, ?, ?, ?, ?, ?)");
        System.out.println(sb);
        return sb.toString();
    }

    public String getCreateTripsTableQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.TRIPS_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_TRIP_SCOOTER_ID);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.SCOOTER_TRIP_USER_ID);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEX);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEY);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONX);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONY);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_STARTTIME);
        sb.append(" timestamptz, ");
        sb.append(DBConstants.SCOOTER_TRIP_ENDTIME);
        sb.append(" timestamptz, ");
        sb.append("primary key  (");
        sb.append(DBConstants.SCOOTER_TRIP_STARTTIME);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_SCOOTER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_USER_ID);
        sb.append("))");
        //System.out.println(sb);
        return  sb.toString();
    }

    public  String getInsertTripsQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.TRIPS_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_TRIP_SCOOTER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_USER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEX);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEY);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONX);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONY);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_STARTTIME);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_ENDTIME);
        sb.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        System.out.println(sb);
        return sb.toString();

    }

    public String getCreateMechanicianTableQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.MECHANICIAN_TABLE);
        sb.append("(");
        sb.append(DBConstants.MECHANICIAN_ID);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.MECHANICIAN_FIRSTNAME);
        sb.append(" char(30), ");
        sb.append(DBConstants.MECHANICIAN_LASTNAME);
        sb.append(" char(30), ");
        sb.append(DBConstants.MECHANICIAN_PASSWORD);
        sb.append(" char(30), ");
        sb.append(DBConstants.MECHANICIAN_PHONE);
        sb.append(" char(30), ");
        sb.append(DBConstants.MECHANICIAN_HIREDATE);
        sb.append(" timestamptz, ");
        sb.append(DBConstants.MECHANICIAN_BANKACCOUNT);
        sb.append(" char(20), ");
        sb.append("primary key (");
        sb.append(DBConstants.MECHANICIAN_ID);
        sb.append("))");
        //System.out.println(sb);
        return sb.toString();
    }

    public String getInsertMechanicianQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.MECHANICIAN_TABLE);
        sb.append("(");
        sb.append(DBConstants.MECHANICIAN_ID);
        sb.append(", ");
        sb.append(DBConstants.MECHANICIAN_LASTNAME);
        sb.append(", ");
        sb.append(DBConstants.MECHANICIAN_FIRSTNAME);
        sb.append(", ");
        sb.append(DBConstants.MECHANICIAN_PASSWORD);
        sb.append(", ");
        sb.append(DBConstants.MECHANICIAN_PHONE);
        sb.append(", ");
        sb.append(DBConstants.MECHANICIAN_HIREDATE);
        sb.append(", ");
        sb.append(DBConstants.MECHANICIAN_BANKACCOUNT);
        sb.append(") VALUES (?, ?, ?, ?, ?, ?, ?)");
        return sb.toString();
    }

    public String getCreateAdressTableQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.ADDRESSE_TABLE);
        sb.append("(");
        sb.append(DBConstants.ADDRESSE_PERSONID);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.ADDRESSE_CITY);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.ADDRESSE_CP);
        sb.append(" integer, ");
        sb.append(DBConstants.ADDRESSE_STREET);
        sb.append(" varchar (50), ");
        sb.append(DBConstants.ADDRESSE_NUMBER);
        sb.append("primary key  (");
        sb.append(DBConstants.ADDRESSE_PERSONID);
        sb.append(" integer ))");

        //System.out.println(sb);
        return sb.toString();
    }

    public String getInsertAddress(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.ADDRESSE_TABLE);
        sb.append("(");
        sb.append(DBConstants.ADDRESSE_PERSONID);
        sb.append(", ");
        sb.append(DBConstants.ADDRESSE_CITY);
        sb.append(", ");
        sb.append(DBConstants.ADDRESSE_CP);
        sb.append(", ");
        sb.append(DBConstants.ADDRESSE_STREET);
        sb.append(", ");
        sb.append(DBConstants.ADDRESSE_NUMBER);
        sb.append(") VALUES (?, ?, ?, ?, ?)");
        System.out.println(sb);
        return sb.toString();
    }

    public String getInsertRegisteredUserTableQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.REGISTERED_USER_TABLE);
        sb.append("(");
        sb.append(DBConstants.REGISTERED_USER_ID);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.REGISTERED_USER_FIRSTNAME);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.REGISTERED_USER_LASTNAME);
        sb.append(" varchar (30), ");
        sb.append(DBConstants.REGISTERED_USER_PHONE);
        sb.append(" varchar (30) ");
        sb.append("primary key, (");
        sb.append(DBConstants.REGISTERED_USER_ID);
        sb.append("))");
        //System.out.println(sb);
        return sb.toString();
    }

    public String getInsertRegistredUserQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.REGISTERED_USER_TABLE);
        sb.append("(");
        sb.append(DBConstants.REGISTERED_USER_ID);
        sb.append(", ");
        sb.append(DBConstants.REGISTERED_USER_FIRSTNAME);
        sb.append(", ");
        sb.append(DBConstants.REGISTERED_USER_LASTNAME);
        sb.append(", ");
        sb.append(DBConstants.REGISTERED_USER_PHONE);
        sb.append(") VALUES (?, ?, ?, ?)");
        return sb.toString();
    }

    public String getInsertRepairQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.REPARATION_TABLE);
        sb.append("(");
        sb.append(DBConstants.REPARATION_SCOOTER);
        sb.append(", ");
        sb.append(DBConstants.REPARATION_USER);
        sb.append(", ");
        sb.append(DBConstants.REPARATION_MECHANIC);
        sb.append(", ");
        sb.append(DBConstants.REPARATION_COMPLAINTIME);
        sb.append(", ");
        sb.append(DBConstants.REPARATION_REPAIRTIME);
        sb.append(", ");
        sb.append(DBConstants.REPARATION_NOTE);
        sb.append(") VALUES (?, ?, ?, ?, ?, ?)");
        System.out.println(sb);
        return sb.toString();
    }

    public String getInsertReloadsQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.RELOADS_TABLE);
        sb.append("(");
        sb.append(DBConstants.RELOADS_SCOOTER);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_USER);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_INITIALLOAD);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_FINALLOAD);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_SOURCEX);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_SOURCEY);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_DESTINATIONX);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_DESTINATIONY);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_STARTTIME);
        sb.append(", ");
        sb.append(DBConstants.RELOADS_ENDTIME);
        sb.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        System.out.println("\n"+sb);
        return sb.toString();
    }

}
