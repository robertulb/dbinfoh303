package model.db;
import model.DateUtil;
import model.Location;
import model.Scooter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ScootersDAO {
    private static ScootersDAO ourInstance = new ScootersDAO();

    public static ScootersDAO getInstance() {
        return ourInstance;
    }

    private ScootersDAO() {
    }

    public  void insertScooters(List<String[]> scooters){
        try {
            scooters.remove(0);

            Connection connection = DBConnection.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    SQLQueryFactory.getInstance().getInsertScooterQuery());
            for(String[] scooter: scooters) {
                preparedStatement.setInt(1, Integer.parseInt(scooter[0]));
                preparedStatement.setTimestamp(2, DateUtil.getInstance().getdate((scooter[1])));
                preparedStatement.setString(3, scooter[2]);
                preparedStatement.setBoolean(4, Boolean.valueOf(scooter[3]));
                preparedStatement.setInt(5, Integer.parseInt(scooter[4]));
                preparedStatement.setBoolean(6, true);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            System.out.println("SCDAO Failed to insert data, Connection issue with dB \n");
        }
        System.out.println("SCDAO scooter intersion done \n");
    }



    public List<Scooter> readAvailableScooters() throws SQLException {
        List<Scooter> scooterData = new ArrayList<>();
        String query = "select * from infoh303.scooters " +
                "where availability = true";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();
        while (resultSet.next()) {
            Scooter scooter = new Scooter(resultSet.getString(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(5),
                    resultSet.getBoolean(6), resultSet.getBoolean(4), getLocation(resultSet.getString(1)));
            scooterData.add(scooter);
        }
        return scooterData;
    }

    public List<Scooter> getScooterComplain() throws SQLException {
        List<Scooter> scooterList = new ArrayList<>();
        String query = "select * " +
                "from infoh303.scooters " +
                "where complain = true ";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement2 = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement2.executeQuery();

        while(resultSet.next()){
            Scooter scooter = new Scooter(resultSet.getString(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(5),
                    resultSet.getBoolean(6), resultSet.getBoolean(4), getLocation(resultSet.getString(1)));
            scooterList.add(scooter);
        }
        return scooterList;
    }

    public List<Scooter> readNonAvailableScooters() throws SQLException {
        List<Scooter> scooterData = new ArrayList<>();
        String query = "select * from infoh303.scooters " +
                "where availability = false";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();

        while (resultSet.next()) {
            String scooterId =resultSet.getString(1);
            Scooter scooter = new Scooter(scooterId, resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(5),
                    resultSet.getBoolean(6), resultSet.getBoolean(4), null);
            scooterData.add(scooter);
        }
        return scooterData;
    }

    public Scooter readDataScooter(String id) throws SQLException {
        String query = "select * from infoh303.scooters " +
                " where scooterid =?";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        ResultSet resultSet =  preparedStatement.executeQuery();
        Scooter scooter = null;

        while (resultSet.next()) {
            scooter = new Scooter(resultSet.getString(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(5),
                    resultSet.getBoolean(6), resultSet.getBoolean(4),
                    getLocation(resultSet.getString(1)));
        }
        return scooter;
    }
/*
    public List<Timestamp> getContactsScooter (String id) throws SQLException {
        List<Timestamp> contacts = new ArrayList<>();
        String query = "select sourcex, sourcey, destinationx, destinationy" +
                " from infoh303.trips " +
                " where scooterid =? ";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        ResultSet resultSet =  preparedStatement.executeQuery();

        while (resultSet.next()) {
            Timestamp sourcex = resultSet.getTimestamp(1);
            Timestamp sourcey = resultSet.getTimestamp(2);
            Timestamp destinationx = resultSet.getTimestamp(3);
            Timestamp destinationy = resultSet.getTimestamp(4);

            contacts.add(sourcex);

            //System.out.println(scooter.getModel());
        }
        return contacts;
    }
*/
    public void insertScooterComplain(String id, boolean complain) throws SQLException {
        String query = "UPDATE infoh303.scooters" +
                "  SET complain = ?" +
                "WHERE scooterid = ?";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setBoolean(1, complain);
        preparedStatement.setString(2, id);
        //affectedrows = preparedStatement.executeUpdate();
        preparedStatement.executeUpdate();
        //return affectedrows;
    }

    private String DistanceMaxOfScooters() throws SQLException {
        String query = "select toto.scooterid as scoot_id, sum(toto.distance) " +
                "from (select trips.scooterid as scooterid, " +
                "2 * 3961 * asin(sqrt((sin(radians((trips.destinationy - trips.sourcey) / 2))) ^ 2 " +
                "+ cos(radians(trips.sourcey)) * cos(radians(trips.destinationy)) " +
                "* (sin(radians((trips.destinationx - trips.sourcex) / 2))) ^ 2)) as distance " +
                "from infoh303.trips) as toto " +
                "group by toto.scooterid " +
                "having (sum(toto.distance) = (select  max(last_table.total) " +
                "from (select new_table.scooterid as scoot_id, " +
                    "sum(new_table.distance) as total " +
                    "from (select trips.scooterid as scooterid, 2 * 3961 * asin(sqrt((sin(radians((trips.destinationy - trips.sourcey) / 2))) ^ 2 " +
                    "+ cos(radians(trips.sourcey)) * cos(radians(trips.destinationy)) " +
                    "* (sin(radians((trips.destinationx - trips.sourcex) / 2))) ^ 2)) as distance " +
                    "from infoh303.trips) as new_table " +
                    "group by new_table.scooterid) as last_table)) ;";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();
        String scooter = "";

        while (resultSet.next()) {
            scooter = resultSet.getString(1);
           }
        return scooter;
    }

    public Scooter getDataDistanceMaxScooter() throws SQLException {
        String scooterid = DistanceMaxOfScooters();
        String query = "select * from infoh303.scooters " +
                        "where scooterid=?";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, scooterid);
        ResultSet resultSet =  preparedStatement.executeQuery();
        Scooter scooter = null;

        while (resultSet.next()) {
            scooter = new Scooter(resultSet.getString(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(5),
                    resultSet.getBoolean(6), resultSet.getBoolean(4), getLocation(resultSet.getString(1)));
        }
        return scooter;
    }
    /*public Scooter getDistancexyEndtimeMaxScooter() throws SQLException {
        String query = "";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();
        while (resultSet.next()) {
            Scooter scooter = new Scooter(resultSet.getString(1), resultSet.getString(2),
                    resultSet.getString(3), resultSet.getInt(5),
                    resultSet.getBoolean(6), resultSet.getBoolean(4), getPosition(resultSet.getString(1)));
        }
        return scooter;
    }*/

    public Location getLocation(String id) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        String query = "select  trips.destinationx,trips.destinationy " +
                        "from infoh303.trips " +
                        "where endtime = " +
                        "(select   max(trips.endtime) " +
                        "from infoh303.trips " +
                        "where trips.scooterid=?)";
        PreparedStatement preparedStatement2 = connection.prepareStatement(query);
        preparedStatement2.setString(1, id);
        ResultSet resultSet = preparedStatement2.executeQuery();
        Location location =null;
        while(resultSet.next()){
            location= new Location(resultSet.getDouble(2), resultSet.getDouble(1));
        }
        return location;
    }

    public void changeBatteryLevel(String id, int status) throws SQLException {
        String query = "update infoh303.scooters " +
                        "set battery_level = ? " +
                        "where scooterid =?;";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, status);
        preparedStatement.setString(2, id);
        preparedStatement.executeUpdate();
    }

    public void insertScooter(String id, String commissioning_date, String model) throws SQLException {
        //todo fonctionne mais vérifier qu'elle existe pas déja
        String query = "insert into infoh303.scooters (scooterid, commissioning_date, model," +
                "complain, battery_level, availability) " +
                "values (?,?,?,?,?,?)" +
                "WHERE scooterid NOT in (SELECT scooterid as scooteriddeux FROM infoh303.scooters ";//+
        //"WHERE scooterid =?);";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryFactory.getInstance().getInsertScooterQuery());
        preparedStatement.setString(1, id);
        preparedStatement.setTimestamp(2, DateUtil.getInstance().getdate(commissioning_date));
        preparedStatement.setString(3, model);
        preparedStatement.setBoolean(4, false);
        preparedStatement.setInt(5, 3);
        preparedStatement.setBoolean(6, false);

        preparedStatement.executeUpdate();
    }

    public void deleteScooter(String id) throws SQLException {
        String query = "delete from infoh303.scooters" +
                " where scooterid =?";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);

        preparedStatement.setString(1, id);

        preparedStatement.executeUpdate();
    }

    public void updateScooterInfo (String id, boolean complain,
                                   int battery_level, boolean availability) throws SQLException {
        String query = "update infoh303.scooters " +
                "set complain = ?, battery_level = ?,availability = ?" +
                " where scooterid =?";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setBoolean(1, complain);
        preparedStatement.setInt(2, battery_level);
        preparedStatement.setBoolean(3, availability);
        preparedStatement.setString(4, id);

        //affectedrows = preparedStatement.executeUpdate();
        preparedStatement.executeUpdate();
    }

    public void closeComplain (String id, boolean complain, boolean availability, String note) throws SQLException {
        String query = "update infoh303.scooters " +
                "set complain = ?, availability =?" +
                " where scooterid =?";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setBoolean(1, complain);
        preparedStatement.setBoolean(2, availability);
        preparedStatement.setString(3, id);

        preparedStatement.executeUpdate();

        String query2 = "update infoh303.reparations " +
                "set note = ? " +
                "where scooterid =?";

        Connection connection2 = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement2 = connection2.prepareStatement(query2);
        connection2.prepareStatement(query2);
        preparedStatement2.setString(1, note);
        preparedStatement2.setString(2, id);

        preparedStatement2.executeUpdate();
    }

}
