package model.db;

import model.DateUtil;
import model.Trip;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.sqrt;

public class TripsDAO {
    private Connection conn;
    ResultSet queryRs;
    PreparedStatement prepStat;

    private static TripsDAO ourInstance = new TripsDAO();

    public static TripsDAO getInstance() {
        return ourInstance;
    }

    private TripsDAO() {}

    public  void insertTrips(List<String[]> trips) {
        try {
            Connection connection = DBConnection.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    SQLQueryFactory.getInstance().getInsertTripsQuery());
            trips.remove(0);
            for(String[] tripRow: trips) {
                preparedStatement.setInt(1, Integer.parseInt(tripRow[0]));
                preparedStatement.setInt(2, Integer.parseInt(tripRow[1]));
                preparedStatement.setDouble(3, Double.parseDouble(tripRow[2]));
                preparedStatement.setDouble(4, Double.parseDouble(tripRow[3]));
                preparedStatement.setDouble(5, Double.parseDouble(tripRow[4]));
                preparedStatement.setDouble(6, Double.parseDouble(tripRow[5]));
                preparedStatement.setTimestamp(7, DateUtil.getInstance().getdate(tripRow[6]));
                preparedStatement.setTimestamp(8, DateUtil.getInstance().getdate(tripRow[7]));
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            e.printStackTrace();
            System.out.println("Failed to insert data, Connection issue with dB");
        }
        System.out.println("trips intersion done ");
    }

    public void tripsQueryResult(String select, String tables){
        String query = "select "+select+" from infoh303."+tables;
        System.out.println("qyery demandé: "+query);

        try {
            this.prepStat = conn.prepareStatement(query);
            queryRs = prepStat.executeQuery();
            //dbr.executeQuery("select scooterid","from infoh303.scooters");

            while (queryRs.next()){
                // tables.add(queryRs.getString(tableSelected));

                System.out.println("Resultset :" + queryRs.getString(tables));
            }
        } catch (SQLException e) {
            System.out.println("Fail to connect to DB" + e.getMessage());
        }
    }

    //--------------------------------------------------------------- queries

    public List<Trip> scooterDeplacementHistory(String id) throws SQLException {
        //todo
        List<Trip> tripList = new ArrayList<>();
        String query = " select  scooterid, userid, starttime, endtime, " +
                "round((point(sourcex, sourcey) <@> point(destinationx,destinationy))::numeric, 3) as miles " +
                "from infoh303.trips where userid=?";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        ResultSet resultSet =  preparedStatement.executeQuery();
        while (resultSet.next()) {
            Trip trip = new Trip(resultSet.getString(2), resultSet.getString(1),
                    resultSet.getFloat(5)/1000, resultSet.getString(3), resultSet.getString(4));

            tripList.add(trip);
        }
        System.out.println(" ddd "+ tripList.size());
        return tripList;
    }
}
