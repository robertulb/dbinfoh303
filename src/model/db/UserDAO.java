package model.db;

import model.RegisteredUser;
import model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private static UserDAO ourInstance = new UserDAO();

    public static UserDAO getInstance() {
        return ourInstance;
    }

    private UserDAO() {
    }

    public  void insertRegistredUsers(List<RegisteredUser> registeredUsers) throws SQLException {
        List<User> users = new ArrayList<>();
        for(RegisteredUser registeredUser: registeredUsers) {
            insertUserGeneralInfo(registeredUser);
            AddressDAO.getInstance().insertAddress(registeredUser.getID(), registeredUser.getAddress());
            users.add(new User(registeredUser.getID(),registeredUser.getPassword() , registeredUser.getBankaccount()));
        }

        insertAnonymousUsers(users);
    }

    public void changeUserToRegistered(RegisteredUser registeredUsers) throws SQLException {
        insertUserGeneralInfo(registeredUsers);
        AddressDAO.getInstance().insertAddress(registeredUsers.getID(), registeredUsers.getAddress());
    }

    public  void insertAnonymousUsers(List<User> users){
        try {
            for(User user: users) {
                insertAnonymousUser(user);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public  void insertAnonymousUser(User user) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryFactory.getInstance().getInsertAnUsersQuery());
        preparedStatement.setInt(1, Integer.parseInt(user.getID()));
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setLong(3, Long.valueOf(user.getBankaccount()));
        preparedStatement.executeUpdate();
    }


    private void insertUserGeneralInfo(RegisteredUser registeredUser) throws SQLException {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueryFactory.getInstance().getInsertRegistredUserQuery());
        preparedStatement.setString(1, registeredUser.getID());
        preparedStatement.setString(2, registeredUser.getFirstName());
        preparedStatement.setString(3, registeredUser.getLastName());
        preparedStatement.setString(4,  registeredUser.getPassword());
        preparedStatement.executeUpdate();
        System.out.println("user created");
    }

    //-----------------------------------------------------------------------------------------------  queries

    // FIXME: 2019-05-11 need to add user to all users
    public  void createRegistredUser(RegisteredUser registeredUser) throws SQLException {
        insertUserGeneralInfo(registeredUser);
        AddressDAO.getInstance().insertAddress(registeredUser.getID(), registeredUser.getAddress());
    }

    public void addAnonymousUser(User user) throws SQLException {
        insertAnonymousUser(user);
    }

    public String readUserByUserID(String id) throws  SQLException {
        String userID = null;
        String query = "select userid, password from infoh303.allusers where userid =?";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        ResultSet resultSet =  preparedStatement.executeQuery();
        while (resultSet.next()) {
           userID = resultSet.getString(1);
        }
        return userID;
    }

    public List<String> authenticate(String id, String password) throws  SQLException {
        List<String> userInfo = new ArrayList<>();
        String query = "select userid, password from infoh303.allusers" +
                " where userid =?";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        ResultSet resultSet =  preparedStatement.executeQuery();

        while (resultSet.next()) {
            userInfo.add(resultSet.getString(1));
            userInfo.add(resultSet.getString(2));
        }
        userInfo.add(getUserLastName(id));
        return userInfo;
    }

    public String getUserLastName(String id) throws SQLException {
        String userInfo = null;
        String query = "select lastname from infoh303.registeredusers" +
                " where userid =?";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        ResultSet resultSet =  preparedStatement.executeQuery();

        while (resultSet.next()) {
            userInfo = resultSet.getString(1);
        }
        return userInfo;
    }

    public void submitComplain (String id, boolean complain) throws SQLException {
        String query = "update infoh303.scooters " +
                "set complain = ?" +
                " where scooterid =?";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setBoolean(1, complain);
        preparedStatement.setString(2, id);
        preparedStatement.executeUpdate();
    }

    public List<User> getAllAnonymeUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        String query = "select *" +
                        " from infoh303.allusers";

        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet =  preparedStatement.executeQuery();

        while (resultSet.next()) {
            User user = new User (resultSet.getString(1),
                                 resultSet.getString(2),
                                 resultSet.getString(3));
            users.add(user);
        }
        System.out.println(users.size());
        return users;
    }

    //---------------------------------------------------------------------------- registeredUser test

    /**
     * @param userid
     * @return list of scooters that are used by user
     * @throws SQLException
     */
    public String getScooterUser(String userid) throws SQLException {
        //todo
        String userInfo = null;
       // String query = "select scooterid from infoh303.scooters, infoh303.allusers" +
         //       " where scooterid =? and userid=?";
        String query = "select scooterid from infoh303.trips "+
                        "where userid=?";
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userid);
        //preparedStatement.setString(2, userid);

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            userInfo = resultSet.getString(1);
            System.out.println(userInfo);
        }
        return userInfo;
    }
}
