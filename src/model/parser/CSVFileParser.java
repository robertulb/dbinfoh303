package model.parser;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CSVFileParser {
    private static CSVFileParser ourInstance = new CSVFileParser();

    public static CSVFileParser getInstance() {
        return ourInstance;
    }

    private CSVFileParser() {

    }

    public List<String[]> parseCSVFileWithCommaSeparator(Reader reader) throws Exception {
        List<String[]> list = new ArrayList<>();
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(0).build();
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            list.add(line);
        }
        reader.close();
        csvReader.close();
        return list;
        }


    public  List<String[]> parseCSVFileWithSemiColonSeparator(Reader reader) throws IOException {
        List<String[]> list = new ArrayList<>();
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';') .withIgnoreQuotations(true) .build();
        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(0) .withCSVParser(parser) .build();
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            list.add(line);
        }
        reader.close();
        csvReader.close();
        return list;
    }
}
