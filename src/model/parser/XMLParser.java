package model.parser;
import model.Person;
import model.PersonFactory;
import model.User;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLParser {
    private  static XMLParser dataParser = new XMLParser();

    private  SAXBuilder saxBuilder;

    private XMLParser(){
        saxBuilder = new SAXBuilder();
    }

    public  List<User> parse(File inputFile) throws JDOMException, IOException {
        List<User> users = new ArrayList<>();
        List<Element> userList = parseFile(inputFile);
        for(int i=0; i<=userList.size()-1; i++) {
            Element user = userList.get(i);
            users.add(new User(user.getChild("ID").getValue(), user.getChild("password").getValue(), user.getChild("bankaccount").getValue()));
        }
        return users;
    }

    public  List<Person> parse(String type, File inputFile) throws JDOMException, IOException {
        List<Element> elements = parseFile(inputFile);
        return  PersonFactory.getInstance().build(type, elements);
    }

    private  List<Element> parseFile(File inputFile) throws JDOMException, IOException {
        return  saxBuilder.build(inputFile).getRootElement().getChildren();
    }

    public static XMLParser getInstance() {
        return dataParser;
    }
}
