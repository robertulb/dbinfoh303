package model;

import controller.ScootersHandler;
import model.db.*;
import model.parser.CSVFileParser;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.util.List;

public class CSVParserTest {
    File file = new File("");
    String csvPath = "resource/";

    @Test
    public void readTripsTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithCommaSeparator(new FileReader(csvPath+"trips.csv"));
        //TripsDAO.getInstance().insertTrips(csvData);
        //TripsDAO.getInstance().scooterDeplacementHistory("0");
        List<Trip> trips = TripsDAO.getInstance().scooterDeplacementHistory("1");
        for (Trip trip: trips) System.out.println(trip);
    }

    @Test
    public void readScootersTest() throws Exception {
        List<String[]> csvData = CSVFileParser.getInstance().parseCSVFileWithSemiColonSeparator(new FileReader(csvPath+"scooters.csv"));
        ScootersDAO.getInstance().insertScooters(csvData);
    }

    @Test
    public void readReloadsTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithCommaSeparator(new FileReader(csvPath+"reloads.csv"));
        ReloadsDAO.getInstance().insertReloadsDAO(csvData);
    }

    @Test
    public void readReparationsTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithCommaSeparator(new FileReader(csvPath+"reparations.csv"));
        ReparationsDAO.getInstance().insertReparationsDAO(csvData);
    }

    private void readLine(List<String[]> csvData) {
        for(String[] rows: csvData){
            System.out.println("--------------------------------------------------------------------------------------------------------");
            for(String str: rows){
                System.out.print( str);
                System.out.print("\t\t");
            }
            System.out.println();
            System.out.println();

        }
    }


}