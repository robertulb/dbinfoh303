package model;

import model.db.MechanicianDAO;
import model.db.UserDAO;
import model.parser.XMLParser;
import org.jdom.JDOMException;
import org.junit.Assert;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class XMLParserTest {

    File file = new File("");
    String csvPath = file.getAbsolutePath()+"/src/model/resource/";

    @Test
    public void parseUserFileTest() throws JDOMException, IOException {
        File inputFile = new File(csvPath+"anonyme_users.xml");
        List<User> users = XMLParser.getInstance().parse(inputFile);
        Assert.assertEquals(544, users.size());
        UserDAO.getInstance().insertAnonymousUsers(users);
    }

    @Test
    public void testRegisteredUser() throws JDOMException, IOException {
        File inputFile = new File(csvPath+"registeredUsers.xml");
        List<Person>  people= XMLParser.getInstance().parse("registeredUser", inputFile);
        Assert.assertEquals(545, people.size());
    }

    @Test
    public void parseMechanicianTest() throws JDOMException, IOException, SQLException {
        File inputFile = new File(csvPath+"mecaniciens.xml");
        List<Person> personList = XMLParser.getInstance().parse("mechanician", inputFile);
        List<Mechanician>  mechanicians = new ArrayList<>();
        for (Person person: personList) {
            mechanicians.add((Mechanician) person);
            System.out.println(person);
        }
        Assert.assertEquals(7, mechanicians.size());
        MechanicianDAO.getInstance().insertMechanicians(mechanicians);
    }

    @Test
    public void parseRegisteredUserTest() throws JDOMException, IOException, SQLException {
        File inputFile = new File(csvPath+"registeredUsers.xml");
        List<Person> registeredUsersList = XMLParser.getInstance().parse("registeredUser",inputFile);
        List<RegisteredUser>  users = new ArrayList<>();
        for (Person user: registeredUsersList)
            users.add((RegisteredUser)user);
        Assert.assertEquals(545, users.size());
       UserDAO.getInstance().insertRegistredUsers(users);
    }
}