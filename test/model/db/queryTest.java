package model.db;

import model.Address;
import model.Location;
import model.RegisteredUser;
import model.Scooter;
import org.junit.Test;
import java.sql.SQLException;
import java.util.List;

public class queryTest {
    //------------------------------------------------------------------- user test

    @Test
    public void getAllAnonymeUsersTest() throws SQLException {
        UserDAO.getInstance().getAllAnonymeUsers();
    }

    @Test
    public void getReadUserByUserIDTest() throws SQLException {
        System.out.println(UserDAO.getInstance().readUserByUserID("4"));
    }

    @Test
    public void authenticateTest() throws SQLException {
        System.out.println(UserDAO.getInstance().authenticate("89","7051"));
    }

    @Test
    public void getUserLastNameTest() throws SQLException {
        System.out.println(UserDAO.getInstance().getUserLastName("0"));
    }

    @Test
    public void submitComplainTest() throws SQLException {
        UserDAO.getInstance().submitComplain("1", true);
    }

    //-------------------------------------------------------------------  scooter test

    @Test
    public void getAvailableScootersTest() throws SQLException {
        Location location =null;
        List<Scooter> scooterList = ScootersDAO.getInstance().readAvailableScooters();
        for(Scooter scooter: scooterList){
           location = ScootersDAO.getInstance().getLocation(scooter.getScooterID().trim());
           System.out.println(location.getLatitude()+" " + location.getLongitude());
        }
    }

    @Test
    public void getNonAvailableScootersTest() throws SQLException {
        List<Scooter> scooterList = ScootersDAO.getInstance().readNonAvailableScooters();
        for(Scooter scooter: scooterList)
            System.out.println(scooter.getScooterID());
    }

    @Test
    public void getReadDataScooterTest() throws SQLException {
        //System.out.println(ScootersDAO.getInstance().readDataScooter("2"));
        System.out.println(ScootersDAO.getInstance().readDataScooter("2").getBatterLevel());
    }

    @Test
    public void insertScooterComplainTest() throws SQLException {
        ScootersDAO.getInstance().insertScooterComplain("2",true);
    }

    @Test
    public void scooterDeplacementHistoryTest() throws SQLException {
        System.out.println(TripsDAO.getInstance().scooterDeplacementHistory("438").size());
    }

    @Test
    public void insertScooterTest() throws SQLException {
        //todo marche mais demande un id diffrent
        ScootersDAO.getInstance().insertScooter("30000","2018-01-01 09:00:00+01","bleuInsert");
    }

    @Test
    public void deleteScooterTest() throws SQLException {
        ScootersDAO.getInstance().deleteScooter("30001");
    }

    @Test
    public void updateScooterInfoTest() throws SQLException {
        ScootersDAO.getInstance().updateScooterInfo("30000",false,
                 2, true);
    }

    @Test
    public void closeComplainTest() throws SQLException {
        boolean dispo = true ;
        ScootersDAO.getInstance().closeComplain("574",dispo, dispo,"");
    }

    @Test
    public void getContactsScooterTest() throws SQLException {
    }

    @Test
    public void getDistancexyEndtimeMaxScooterTest() throws SQLException {
        //System.out.println(ScootersDAO.getInstance().getDistancexyEndtimeMaxScooter());
    }

    //------------------------------------------------------------------- registereduser test
    @Test
    public void getScooterUserTest() throws SQLException {
        UserDAO.getInstance().getScooterUser("3");
    }

    @Test
    public void changeUserTRegisteredTest() throws SQLException {
        RegisteredUser rg = new RegisteredUser("test","test","test",
                                            new Address("t",4,"t",3),
                                            "test","test","test");
        UserDAO.getInstance().changeUserToRegistered(rg);
    }

     @Test
    public void changeBatteryLevelTest() throws SQLException {
        ScootersDAO.getInstance().changeBatteryLevel("4", 50);
    }

    //--------------------------------------------------------------------  reparations queries

    @Test
    public void updateComplainTest() throws SQLException {
        ReparationsDAO.getInstance().updateComplain("r",
                " problme roues", "03:35:09+01:00");
    }

    @Test
    public void getScooterComplainTest() throws SQLException {
        List<Scooter> scooterList = ScootersDAO.getInstance().getScooterComplain();
        for(Scooter scooter: scooterList)
            System.out.println(scooter.getScooterID());
    }

    //--------------------------------------------------------------------  5 queries
    // 1
    @Test
    public void readAvailableScootersFiveQueryTest() throws SQLException {
        System.out.println(FiveRequest.getInstance().readAvailableScootersFiveQuery().size());
    }

    @Test// 3
    public void getDistanceMaxScooterTest() throws SQLException {
        System.out.println(ScootersDAO.getInstance().getDataDistanceMaxScooter());
        //ScootersDAO.getInstance().getDataDistanceMaxScooter().getModel();
    }

    @Test// 4
    public void scooterWithMore4complainTest() throws SQLException {
        System.out.println(FiveRequest.getInstance().scooterWithMore4complain().size());
    }

    @Test// 5
    public void tenTripsUsersTest() throws SQLException {
        System.out.println(FiveRequest.getInstance().getTenTripsUser());
    }
}